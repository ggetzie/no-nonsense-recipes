module rotd

go 1.15

require (
	github.com/aws/aws-lambda-go v1.22.0 // direct
	github.com/dghubble/go-twitter v0.0.0-20201011215211-4b180d0cc78d // direct
	github.com/dghubble/oauth1 v0.7.0 // direct
	github.com/jackc/pgx/v4 v4.10.1 // direct
)
